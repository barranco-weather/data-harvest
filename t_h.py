import Adafruit_DHT
import Adafruit_BMP.BMP085 as BMP085 # Imports the BMP library
import time
import sys
import requests
from gpiozero import MCP3008

sensor = 11    # DHT11
pin = 4    # Connected to GPIO4
uvsensor = MCP3008(0)  # Object representing analogue UV sensor
pressureSensor = BMP085.BMP085() #Object representing BMP180 
msmts = int(sys.argv[1])    # Number of iterations
sleep_time = int(sys.argv[2])

url = "http://www.innovations.pe/arturo/api-bweather/measures/create/"


def checkBackupFile(backupname):
    
    backup = open(backupname, "r")
    if backup.read(1):  # If backup.txt file isn't empty
        toerase = []
        alllines = backup.readlines()
        c = 0
        for data in alllines:
            try:
                r = requests.post(url, data.rstrip('\n'))
                if r.status_code == 200:
                    print "Datos enviados del backup"
                    toerase.append(c)  # If successfully, record data line
            except:
                pass
            c += 1
        backup.close()
        # To delete data already sent
        backup = open(backupname, "w")  # Write mode resets file
        for i in range(len(alllines)):
            if i not in toerase:  # Only write data that couldn't be sent
                backup.write(alllines[i])
    else:
        print "Backup vacio"


while True:
    # Check if there is any data in queue
    checkBackupFile("backup.txt")

    acc_t = 0
    acc_h = 0
    acc_uv = 0
    acc_p = 0
    for i in range(msmts):
        humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
        uv = uvsensor.value
        pressure = pressureSensor.read_pressure()
        acc_t += temperature
        acc_h += humidity
        acc_uv += uv
        acc_p += pressure
        sys.stdout.write("Measure #" + str(i+1) + " -> " + str(temperature) +
                         "C - " + str(humidity) + "RH - " + str(uv) + "nm - " + str(pressure) + "atm\n")

    prom_temp = acc_t/msmts
    prom_hum = acc_h/msmts
    prom_uv = acc_uv/msmts
    prom_p = acc_p/msmts

    sys.stdout.write("Temp: " + str(prom_temp) + " C\n")
    sys.stdout.write("Hum: " + str(prom_hum) + " RH\n")
    sys.stdout.write("UV: " + str(prom_uv) + " nm\n")
    sys.stdout.write("Pres: " + str(prom_p) + " atm\n")
    print "END after ",str(msmts)," measurements"

    data = '{"temperature":' + str(prom_temp) + ', "rel_humidity": ' + str(prom_hum) + ',"uv":' + str(prom_uv) + ',"bmp":' + str(prom_p)+'}'
    print data
    
    try:
        r = requests.post(url, data)
        if r.status_code == 200:
            print("Data sent successfully")
        else:  # If data can't be sent
            # Store data in backup.txt file
            print "Unnable to connect to server. Saving data in backup.txt"
            backup = open("backup.txt", "w")
            bkcup = data + "\n"
            print bkcup
            backup.write(bkcup)
            backup.close()
            
    except Exception:
        # Store data in backup.txt file
        print "Unnable to connect to server. Saving data in backup.txt"
        backup = open("backup.txt", "a")
        bkcup = data + "\n"
        print bkcup
        backup.write(bkcup)
        backup.close()
        
    time.sleep(sleep_time)
